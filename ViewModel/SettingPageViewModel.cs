﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Pages;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class SettingPageViewModel : BindableBase
  {
    private readonly MessageBus _messageBus;
    private readonly FileService _fileService;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<Setting> Settings { get; set; } = new ObservableCollection<Setting>();

    public SettingPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus, FileService fileService)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      this._fileService = fileService;
      Settings = _unitOfWork.Settings.GetAll().ToObservableCollection();      
    }

    public ICommand Add => new AsyncCommand(async () =>
    {
      await _messageBus.SendTo<EditSettingPageViewModel>(new Message(new Setting()));      
      this._navigation.Navigate(new EditSettingPage());
    });

    public ICommand Import => new DelegateCommand(() =>
    {
      //this._navigation.Navigate(new ImportSettingPage());
    });

    public ICommand Edit => new AsyncCommand<Setting>(async (setting) =>
    {
      await _messageBus.SendTo<EditSettingPageViewModel>(new Message(setting));
      _navigation.Navigate(new EditSettingPage());
    });

    public ICommand Delete => new DelegateCommand<Setting>((setting) =>
    {
      this._unitOfWork.Settings.Delete(setting.Id);
      this.Settings = _unitOfWork.Settings.GetAll().ToObservableCollection();
    }, (setting) => setting != null);

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);
  }
}
