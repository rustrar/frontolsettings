﻿using DevExpress.Mvvm;
using System.Windows.Controls;
using System.Windows.Input;
using WpfApp.Pages;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class MainViewModel : BindableBase
  {
    private readonly PageService _navigation;

    public Page CurrentPage { get; set; }

    public MainViewModel(PageService navigation)
    {
      navigation.OnPageChanged += page => CurrentPage = page;
      this._navigation = navigation;
    }

    public ICommand OpenCashboxes => new DelegateCommand(() =>
    {
      this._navigation.Navigate(new CashboxPage());
    });

    public ICommand OpenFtp => new DelegateCommand(() =>
    {
      this._navigation.Navigate(new FtpPage());
    });

    public ICommand OpenSettings => new DelegateCommand(() =>
    {
      this._navigation.Navigate(new SettingPage());
    });

    public ICommand OpenAddresses => new DelegateCommand(() =>
    {
      this._navigation.Navigate(new AddressPage());
    });

    public ICommand Exit => new DelegateCommand(() =>
    {
      System.Windows.Application.Current.Shutdown();
    });
  }
}
