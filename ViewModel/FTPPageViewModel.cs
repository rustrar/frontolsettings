﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Pages;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class FTPPageViewModel : BindableBase
  {
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<FTPStorage> FTPStorages { get; set; } = new ObservableCollection<FTPStorage>();

    public FTPPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._messageBus = messageBus;
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      FTPStorages = _unitOfWork.FTPStorages.GetAll().ToObservableCollection();
    }

    public ICommand Add => new AsyncCommand(async () =>
    {
      await _messageBus.SendTo<EditFtpPageViewModel>(new Message(new FTPStorage()));
      this._navigation.Navigate(new EditFtpPage());
    });

    public ICommand Edit => new AsyncCommand<FTPStorage>(async (fTPStorage) =>
    {
      await _messageBus.SendTo<EditFtpPageViewModel>(new Message(fTPStorage));
      _navigation.Navigate(new EditFtpPage());
    });

    public ICommand Delete => new DelegateCommand<FTPStorage>((fTPStorage) =>
    {
      this._unitOfWork.FTPStorages.Delete(fTPStorage.Id);
      this.FTPStorages = _unitOfWork.FTPStorages.GetAll().ToObservableCollection(); 
    }, (fTPStorage) => fTPStorage != null);

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

  }
}
