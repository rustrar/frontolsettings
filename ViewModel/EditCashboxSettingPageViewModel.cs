﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class EditCashboxSettingPageViewModel : BindableBase
  {
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public MessageViewModel ErrorMessageViewModel { get; } = new MessageViewModel();
    public string ErrorMessage
    {
      set => ErrorMessageViewModel.Message = value;
    }
    public MessageViewModel StatusMessageViewModel { get; } = new MessageViewModel();
    public string StatusMessage
    {
      set => StatusMessageViewModel.Message = value;
    }
    public CashboxSetting CashboxSetting { get; set; } = new CashboxSetting();
    public Cashbox SelectedCashbox => CashboxSetting.Cashbox;
    public ObservableCollection<Setting> Settings => _unitOfWork.Settings.GetAll().ToObservableCollection();
    public ObservableCollection<Cashbox> Cashboxes => _unitOfWork.Cashboxes.GetAll().ToObservableCollection();
    public ObservableCollection<CashboxSetting> CashboxSettings => _unitOfWork.CashboxSettings.GetAll().ToObservableCollection();

    public EditCashboxSettingPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      _messageBus.Receive<Message>(this, async message =>
      {
        CashboxSetting = (CashboxSetting)message.Data;
      });
    }

    public ICommand Save => new DelegateCommand(() =>
    {
      this.ClearMessages();

      try
      {
        _unitOfWork.CashboxSettings.Save(this.CashboxSetting);
        this.StatusMessage = "Настройка сохранена";
      }
      catch (System.Exception)
      {
        this.ErrorMessage = "Ошибка сохранения";
      }
    }, () => this.CashboxSetting.IsValid());

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this.ClearMessages();
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

    private void ClearMessages()
    {
      this.StatusMessage = string.Empty;
      this.ErrorMessage = string.Empty;
    }
  }
}