﻿using DevExpress.Mvvm;

namespace WpfApp.ViewModel
{
  public class MessageViewModel : BindableBase
  {
    public string Message { get; set; }
    public bool HasMessage => !string.IsNullOrEmpty(Message);
  }
}
