﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Providers;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class EditFtpPageViewModel : BindableBase
  {
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly FtpService _ftpProvider;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<FTPStorage> FTPStorages { get; set; } = new ObservableCollection<FTPStorage>();
    public MessageViewModel ErrorMessageViewModel { get; }

    public string ErrorMessage
    {
      set => ErrorMessageViewModel.Message = value;
    }

    public MessageViewModel StatusMessageViewModel { get; }

    public string StatusMessage
    {
      set => StatusMessageViewModel.Message = value;
    }

    public FTPStorage NewFTPStorage { get; set; } = new FTPStorage();

    public EditFtpPageViewModel(PageService navigation, FtpService ftpProvider, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._ftpProvider = ftpProvider;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      ErrorMessageViewModel = new MessageViewModel();
      StatusMessageViewModel = new MessageViewModel();
      FTPStorages = _unitOfWork.FTPStorages.GetAll().ToObservableCollection();
      _messageBus.Receive<Message>(this, async message =>
      {
        NewFTPStorage = (FTPStorage)message.Data;
      });
  
    }

    public ICommand Save => new DelegateCommand(() =>
    {
      this.ClearMessages();
      try
      {
        _unitOfWork.FTPStorages.Save(NewFTPStorage);
        FTPStorages.Add(NewFTPStorage);
        this.StatusMessage = "FTP-соединение сохранено";
      }
      catch (System.Exception)
      {
        this.ErrorMessage = "Ошибка сохранения";
      }
    }, () => NewFTPStorage.IsValid());

    public ICommand TestFtpConnection => new AsyncCommand(async () =>
    {
      this.ClearMessages();
      this._ftpProvider.Init(NewFTPStorage);
      var checkConnectionResult = await this._ftpProvider.CheckConnectionAsync();
      if (checkConnectionResult.Item1)
        this.StatusMessage = $"Соединение установлено!\n{checkConnectionResult.Item2}";
      else
      {
        this.ErrorMessage = $"Ошибка соединения.\n{checkConnectionResult.Item2}";
      }
    });

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this.ClearMessages();
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

    private void ClearMessages()
    {
      this.StatusMessage = string.Empty;
      this.ErrorMessage = string.Empty;
    }
  }
}
