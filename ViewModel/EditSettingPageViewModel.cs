﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class EditSettingPageViewModel : BindableBase
  {
    public ObservableCollection<Setting> Settings { get; set; } = new ObservableCollection<Setting>();

    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public MessageViewModel ErrorMessageViewModel { get; }

    public string ErrorMessage
    {
      set => ErrorMessageViewModel.Message = value;
    }

    public MessageViewModel StatusMessageViewModel { get; }

    public string StatusMessage
    {
      set => StatusMessageViewModel.Message = value;
    }

    public Setting NewSetting { get; set; } = new Setting();
    

    public EditSettingPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      ErrorMessageViewModel = new MessageViewModel();
      StatusMessageViewModel = new MessageViewModel();
      Settings = _unitOfWork.Settings.GetAll().ToObservableCollection();   
      _messageBus.Receive<Message>(this, async message =>
      {
        NewSetting = (Setting)message.Data;
      });
    }

    public ICommand Save => new AsyncCommand(async () =>
    {
      this.ClearMessages();

      try
      {
        _unitOfWork.Settings.Save(this.NewSetting);
        this.StatusMessage = "Настройка сохранена";
      }
      catch (System.Exception)
      {
        this.ErrorMessage = "Ошибка сохранения";
      }
    }, () => this.NewSetting.IsValid());

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this.ClearMessages();
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

    private void ClearMessages()
    {
      this.StatusMessage = string.Empty;
      this.ErrorMessage = string.Empty;
    }
  }
}
