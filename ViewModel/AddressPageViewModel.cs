﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Pages;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class AddressPageViewModel : BindableBase
  {    
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<Address> Addresses { get; set; } = new ObservableCollection<Address>();

    public AddressPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      Addresses = _unitOfWork.Addresses.GetAll().ToObservableCollection();
    }

    public ICommand Add => new AsyncCommand(async () =>
    {
      await _messageBus.SendTo<EditAddressPageViewModel>(new Message(new Address()));
      this._navigation.Navigate(new EditAddressPage());
    });

    public ICommand Edit => new AsyncCommand<Address>(async (address) =>
    {
      await _messageBus.SendTo<EditAddressPageViewModel>(new Message(address));
      _navigation.Navigate(new EditAddressPage());
    });

    public ICommand Delete => new DelegateCommand<Address>((address) =>
    {
      this._unitOfWork.Addresses.Delete(address.Id);
      this.Addresses = _unitOfWork.Addresses.GetAll().ToObservableCollection();
    }, (setting) => setting != null);

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);
  }
}
