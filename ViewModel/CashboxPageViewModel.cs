﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Pages;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class CashboxPageViewModel : BindableBase
  {
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<Cashbox> Cashboxes { get; set; } = new ObservableCollection<Cashbox>();
    public ObservableCollection<CashboxSetting> SelectedCashboxSettings { get; set; } = new ObservableCollection<CashboxSetting>();
    public Cashbox SelectedCashbox { get; set; } = new Cashbox();
    public CashboxPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      Cashboxes = _unitOfWork.Cashboxes.GetAll().ToObservableCollection();
      SelectedCashbox = Cashboxes.FirstOrDefault();
      SelectedCashboxSettings = SelectedCashbox.CashboxSettings.ToObservableCollection();
    }
    
    public ICommand AddCashbox => new AsyncCommand(async () =>
    {
      await _messageBus.SendTo<EditCashboxPageViewModel>(new Message(new Cashbox()));
      this._navigation.Navigate(new EditCashboxPage());
    });

    public ICommand EditCashbox => new AsyncCommand<Cashbox>(async (cashbox) =>
    {
      await _messageBus.SendTo<EditCashboxPageViewModel>(new Message(cashbox));
      _navigation.Navigate(new EditCashboxPage());
    });

    public ICommand DeleteCashbox => new DelegateCommand<Cashbox>((cashbox) =>
    {
      this._unitOfWork.Cashboxes.Delete(cashbox.Id);
      this.Cashboxes = _unitOfWork.Cashboxes.GetAll().ToObservableCollection();
    }, (cashbox) => cashbox != null);

    public ICommand AddCashboxSetting => new AsyncCommand(async () =>
    {
      var newCashboxSetting = new CashboxSetting();
      newCashboxSetting.Cashbox = SelectedCashbox;
      await _messageBus.SendTo<AddCashboxSettingPageViewModel>(new Message(newCashboxSetting));
      this._navigation.Navigate(new AddCashboxSettingPage());
    });

    public ICommand EditCashoxSetting => new AsyncCommand<CashboxSetting>(async (cashboxSetting) =>
    {
      await _messageBus.SendTo<EditCashboxSettingPageViewModel>(new Message(cashboxSetting));
      _navigation.Navigate(new EditCashboxSettingPage());
    });

    public ICommand DeleteCashboxSetting => new DelegateCommand<CashboxSetting>((cashboxSetting) =>
    {
      this._unitOfWork.CashboxSettings.Delete(cashboxSetting.Id);
      SelectedCashboxSettings = this.SelectedCashbox.CashboxSettings.ToObservableCollection();
    }, (cashboxSetting) => cashboxSetting != null);

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);
  }
}
