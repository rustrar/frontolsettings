﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class EditAddressPageViewModel : BindableBase
  {
    public ObservableCollection<Address> Addresses { get; set; } = new ObservableCollection<Address>();

    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public MessageViewModel ErrorMessageViewModel { get; }

    public string ErrorMessage
    {
      set => ErrorMessageViewModel.Message = value;
    }

    public MessageViewModel StatusMessageViewModel { get; }

    public string StatusMessage
    {
      set => StatusMessageViewModel.Message = value;
    }

    public Address NewAddress { get; set; } = new Address();


    public EditAddressPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      ErrorMessageViewModel = new MessageViewModel();
      StatusMessageViewModel = new MessageViewModel();
      Addresses = _unitOfWork.Addresses.GetAll().ToObservableCollection();
      _messageBus.Receive<Message>(this, async message =>
      {
        NewAddress = (Address)message.Data;
      });
    }

    public ICommand Save => new DelegateCommand(() =>
    {
      this.ClearMessages();

      try
      {
        _unitOfWork.Addresses.Save(this.NewAddress);
        this.StatusMessage = "Настройка сохранена";
      }
      catch (System.Exception)
      {
        this.ErrorMessage = "Ошибка сохранения";
      }
    }, () => this.NewAddress.IsValid());

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this.ClearMessages();
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

    private void ClearMessages()
    {
      this.StatusMessage = string.Empty;
      this.ErrorMessage = string.Empty;
    }
  }
}
