﻿using DevExpress.Mvvm;
using DevExpress.Mvvm.Native;
using System.Collections.ObjectModel;
using System.Windows.Input;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Services;

namespace WpfApp.ViewModel
{
  public class EditCashboxPageViewModel
  {
    private readonly MessageBus _messageBus;
    private readonly PageService _navigation;
    private readonly UnitOfWork _unitOfWork;

    public ObservableCollection<Cashbox> Cashboxes { get; set; } = new ObservableCollection<Cashbox>();
    public ObservableCollection<FTPStorage> FTPStorages => _unitOfWork.FTPStorages.GetAll().ToObservableCollection();   
    public ObservableCollection<Address> Addresses => _unitOfWork.Addresses.GetAll().ToObservableCollection();    
    public Cashbox NewCashbox { get; set; }    
    public MessageViewModel ErrorMessageViewModel { get; }
    public string ErrorMessage
    {
      set => ErrorMessageViewModel.Message = value;
    }
    public MessageViewModel StatusMessageViewModel { get; }
    public string StatusMessage
    {
      set => StatusMessageViewModel.Message = value;
    }

    public EditCashboxPageViewModel(PageService navigation, UnitOfWork unitOfWork, MessageBus messageBus)
    {
      this._navigation = navigation;
      this._unitOfWork = unitOfWork;
      this._messageBus = messageBus;
      ErrorMessageViewModel = new MessageViewModel();
      StatusMessageViewModel = new MessageViewModel();
      Cashboxes = _unitOfWork.Cashboxes.GetAll().ToObservableCollection();
      _messageBus.Receive<Message>(this, async message =>
      {
        NewCashbox = (Cashbox)message.Data;
      });

    }

    public ICommand Save => new DelegateCommand(() =>
    {
      this.ClearMessages();

      try
      {
        _unitOfWork.Cashboxes.Save(NewCashbox);
        this.StatusMessage = "Изменения сохранены";
      }
      catch (System.Exception e)
      {
        this.ErrorMessage = "Ошибка сохранения";
      }
    }, () => NewCashbox.IsValid());

    public ICommand GoToBack => new DelegateCommand(() =>
    {
      this.ClearMessages();
      this._navigation.GoToBack();
    }, () => _navigation.CanGoToBack);

    private void ClearMessages()
    {
      this.StatusMessage = string.Empty;
      this.ErrorMessage = string.Empty;
    }
  }
}
