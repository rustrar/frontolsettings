﻿using System.Collections.Generic;

namespace WpfApp.Data
{
  public interface IRepository<T> where T : class
  {
    IEnumerable<T> GetAll();
    T Get(int id);
    void Save(T item);
    T Delete(int id);
  }
}