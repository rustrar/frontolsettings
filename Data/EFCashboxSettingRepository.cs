﻿using System.Collections.Generic;
using System.Linq;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class EFCashboSettingRepository : IRepository<CashboxSetting>
  {
    private SQLiteDBContext context;
    public EFCashboSettingRepository(SQLiteDBContext ctx)
    {
      context = ctx;
    }

    public CashboxSetting Get(int id)
    {
      return context.CashboxSettings.Find(id);
    }

    public IEnumerable<CashboxSetting> GetAll()
    {
      return context.CashboxSettings;
    }

    public void Save(CashboxSetting cashboxSetting)
    {
      if (cashboxSetting.Id == 0)
        context.CashboxSettings.Add(cashboxSetting);

      else
      {
        CashboxSetting dbEntry = context.CashboxSettings
            .FirstOrDefault(p => p.Id == cashboxSetting.Id);
        if (dbEntry != null)
        {
          dbEntry.Cashbox = cashboxSetting.Cashbox;
          dbEntry.Setting = cashboxSetting.Setting;
          dbEntry.Value = cashboxSetting.Value;
        }
      }
      context.SaveChanges();
    }
    
    public CashboxSetting Delete(int id)
    {
      CashboxSetting dbEntry = context.CashboxSettings
        .FirstOrDefault(p => p.Id == id);
      if (dbEntry != null)
      {
        context.CashboxSettings.Remove(dbEntry);
        context.SaveChanges();
      }
      return dbEntry;
    }
  }
}
