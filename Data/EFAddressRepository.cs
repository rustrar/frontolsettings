﻿using System.Collections.Generic;
using System.Linq;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class EFAddressRepository : IRepository<Address>
  {
    private SQLiteDBContext context;
    public EFAddressRepository(SQLiteDBContext ctx)
    {
      context = ctx;
    }

    public Address Get(int id)
    {
      return context.Addresses.Find(id);
    }

    public IEnumerable<Address> GetAll()
    {
      return context.Addresses;
    }

    public void Save(Address address)
    {
      if (address.Id == 0)
        context.Addresses.Add(address);

      else
      {
        Address dbEntry = context.Addresses
            .FirstOrDefault(p => p.Id == address.Id);
        if (dbEntry != null)
        {
          dbEntry.City = address.City;
          dbEntry.Street = address.Street;
          dbEntry.Number = address?.Number;
        }
      }
      context.SaveChanges();
    }
    
    public Address Delete(int id)
    {
      Address dbEntry = context.Addresses.FirstOrDefault(p => p.Id == id);
      if (dbEntry != null)
      {
        context.Addresses.Remove(dbEntry);
        context.SaveChanges();
      }
      return dbEntry;
    }
  }
}
