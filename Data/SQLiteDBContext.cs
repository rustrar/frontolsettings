﻿using Microsoft.EntityFrameworkCore;
using System.IO;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class SQLiteDBContext : DbContext
  {
    #region Constructor
    public SQLiteDBContext() : base()
    {
      Database.EnsureCreated();
    }

    public SQLiteDBContext(DbContextOptions options) : base(options)
    {
      Database.EnsureCreated();
    }
    #endregion

    #region Public properties
    public DbSet<Address> Addresses { get; set; }
    public DbSet<Cashbox> Cashboxes { get; set; }
    public DbSet<Setting> Settings { get; set; }
    public DbSet<FTPStorage> FTPStorages { get; set; }
    public DbSet<CashboxSetting> CashboxSettings { get; set; }
    #endregion

    #region Overridden methods    
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
      var dbPath = Path.Combine("frontolsettings.db");
      options.UseSqlite("Filename = " + dbPath);
      base.OnConfiguring(options);
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Cashbox>()
        .HasMany(s => s.Settings)
        .WithMany(c => c.Cashboxes)
        .UsingEntity<CashboxSetting>(
          j => j.HasOne(pt => pt.Setting)
            .WithMany(t => t.CashboxSettings)
            .HasForeignKey(pt => pt.SettingId),
          j => j.HasOne(pt => pt.Cashbox)
            .WithMany(p => p.CashboxSettings)
            .HasForeignKey(pt => pt.CashboxId),
          j =>
          {
            j.Property(pt => pt.Id).ValueGeneratedOnAdd();
            j.Property(pt => pt.Value);
            j.HasKey(t => t.Id);
            j.ToTable("CashboxSettings");
          }
      );
    }
    #endregion
  }
}
