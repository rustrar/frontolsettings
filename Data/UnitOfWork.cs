﻿using System;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class UnitOfWork : IDisposable
  {
    private SQLiteDBContext _db;
    private IRepository<Setting> _settingRepository;
    private IRepository<FTPStorage> _ftpRepository;
    private IRepository<Cashbox> _cashboxRepository;
    private IRepository<Address> _addressRepository;
    private IRepository<CashboxSetting> _cashboxSettingRepository;
    private bool disposed = false;

    public UnitOfWork(SQLiteDBContext db, 
      IRepository<Setting> settingRepository, 
      IRepository<FTPStorage> ftpRepository, 
      IRepository<Cashbox> cashboxRepository,
      IRepository<Address> addressRepository,
      IRepository<CashboxSetting> cashboxSettingRepository)
    {
      this._db = db;
      this._settingRepository = settingRepository;
      this._ftpRepository = ftpRepository;
      this._cashboxRepository = cashboxRepository;
      this._addressRepository = addressRepository;
      this._cashboxSettingRepository = cashboxSettingRepository;
    }

    public IRepository<Setting> Settings
    {
      get
      {
        if (_settingRepository == null)
          _settingRepository = new EFSettingRepository(_db);
        return _settingRepository;
      }
    }

    public IRepository<FTPStorage> FTPStorages
    {
      get
      {
        if (_ftpRepository == null)
          _ftpRepository = new EFFTPStorageRepository(_db);
        return _ftpRepository;
      }
    }

    public IRepository<Cashbox> Cashboxes
    {
      get
      {
        if (_cashboxRepository == null)
          _cashboxRepository = new EFCashboxRepository(_db);
        return _cashboxRepository;
      }
    }

    public IRepository<Address> Addresses
    {
      get
      {
        if (_addressRepository == null)
          _addressRepository= new EFAddressRepository(_db);
        return _addressRepository;
      }
    }

    public IRepository<CashboxSetting> CashboxSettings
    {
      get
      {
        if (_cashboxSettingRepository== null)
          _cashboxSettingRepository = new EFCashboSettingRepository(_db);
        return _cashboxSettingRepository;
      }
    }

    public virtual void Dispose(bool disposing)
    {
      if (!this.disposed)
      {
        if (disposing)        
          _db.Dispose();

        this.disposed = true;
      }
    }

    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }


  }
}