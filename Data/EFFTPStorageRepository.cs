﻿using System.Collections.Generic;
using System.Linq;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class EFFTPStorageRepository : IRepository<FTPStorage>
  {
    private SQLiteDBContext context;
    public EFFTPStorageRepository(SQLiteDBContext ctx)
    {
      this.context = ctx;
    }

    public FTPStorage Get(int id)
    {
      return this.context.FTPStorages.Find(id);
    }

    public IEnumerable<FTPStorage> GetAll()
    {
      return this.context.FTPStorages;
    }

    public void Save(FTPStorage ftpStorage)
    {
      if (ftpStorage.Id == 0)
        context.FTPStorages.Add(ftpStorage);
      else
      {
        FTPStorage dbEntry = context.FTPStorages
            .FirstOrDefault(p => p.Id == ftpStorage.Id);
        if (dbEntry != null)
        {
          dbEntry.Name = ftpStorage.Name;
          dbEntry.Url = ftpStorage.Url;
          dbEntry.Port = ftpStorage.Port;
          dbEntry.Login = ftpStorage.Login;
          dbEntry.Password = dbEntry.Password;
        }
      }
      context.SaveChanges();
    }

    public FTPStorage Delete(int id)
    {
      FTPStorage dbEntry = context.FTPStorages.FirstOrDefault(p => p.Id == id);
      if (dbEntry != null)
      {
        context.FTPStorages.Remove(dbEntry);
        context.SaveChanges();
      }
      return dbEntry;
    }
  }
}
