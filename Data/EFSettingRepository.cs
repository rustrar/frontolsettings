﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class EFSettingRepository : IRepository<Setting>
  {
    private SQLiteDBContext context;
    public EFSettingRepository(SQLiteDBContext ctx)
    {
      context = ctx;
    }

    public Setting Get(int id)
    {
      return this.context.Settings.Find(id);
    }

    public IEnumerable<Setting> GetAll()
    {
      return this.context.Settings
        .Include(x => x.Cashboxes);
    }

    public void Save(Setting setting)
    {
      if (setting.Id == 0)
        context.Settings.Add(setting);
      else
      {
        Setting dbEntry = context.Settings
            .FirstOrDefault(p => p.Id == setting.Id);

        if (dbEntry != null)
          dbEntry.Name = setting.Name;

      }
      context.SaveChanges();
    }

    public Setting Delete(int id)
    {
      Setting dbEntry = context.Settings.FirstOrDefault(p => p.Id == id);
      if (dbEntry != null)
      {
        context.Settings.Remove(dbEntry);
        context.SaveChanges();
      }
      return dbEntry;
    }
  }
}