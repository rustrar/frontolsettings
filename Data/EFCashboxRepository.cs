﻿using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using WpfApp.Model;

namespace WpfApp.Data
{
  public class EFCashboxRepository : IRepository<Cashbox>
  {
    private SQLiteDBContext context;
    public EFCashboxRepository(SQLiteDBContext ctx)
    {
      context = ctx;
    }

    public Cashbox Get(int id)
    {
      return context.Cashboxes
        .Include(x => x.Settings)
        .Include(x => x.Address)
        .Include(x => x.FTPStorage)
        .FirstOrDefault();
    }

    public IEnumerable<Cashbox> GetAll()
    {
      return context.Cashboxes
        .Include(x => x.Settings)
        .Include(x => x.Address)
        .Include(x => x.FTPStorage);
    }

    public void Save(Cashbox cashbox)
    {
      if (cashbox.Id == 0)
        context.Cashboxes.Add(cashbox);
      else
      {
        Cashbox dbEntry = context.Cashboxes
            .FirstOrDefault(p => p.Id == cashbox.Id);
        if (dbEntry != null)
        {
          dbEntry.Name = cashbox.Name;
          dbEntry.Address = cashbox.Address;
          dbEntry.Settings = cashbox.Settings;
        }
      }
      context.SaveChanges();
    }

    public Cashbox Delete(int id)
    {
      Cashbox dbEntry = context.Cashboxes.FirstOrDefault(p => p.Id == id);
      if (dbEntry != null)
      {
        context.Cashboxes.Remove(dbEntry);
        context.SaveChanges();
      }
      return dbEntry;
    }
  }
}
