﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace WpfApp.Model
{
  public abstract class PropertyValidateModel : IDataErrorInfo
  {
    public string Error { get { return null; } }

    public string this[string columnName]
    {
      get
      {
        var validationResults = new List<ValidationResult>();

        if (Validator.TryValidateProperty(
                GetType().GetProperty(columnName).GetValue(this)
                , new ValidationContext(this)
                {
                  MemberName = columnName
                }
                , validationResults))
          return null;

        return validationResults.First().ErrorMessage;
      }
    }

    public bool IsValid()
    {
      var validationResults = new List<ValidationResult>();
      return Validator.TryValidateObject(this, new ValidationContext(this), validationResults, true);        
    }
  }
}