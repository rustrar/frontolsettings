﻿using System.ComponentModel.DataAnnotations;

namespace WpfApp.Model
{
  /// <summary>
  /// FTP-хранилище.
  /// </summary>
  public class FTPStorage : PropertyValidateModel
  {
    /// <summary>
    /// Идентификатор.
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Имя хранилища.
    /// </summary>
    [Required(ErrorMessage = "Заполните имя соединения")]
    public string Name { get; set; }

    /// <summary>
    /// Url-адрес FTP-хранилища.
    /// </summary>
    [Required(ErrorMessage = "Заполните адрес соединения")]
    public string Url { get; set; }

    /// <summary>
    /// Порт FTP-хранилища.
    /// </summary>    
    public int Port { get; set; } = 21;

    /// <summary>
    /// Логин для подключения к FTP-хранилищу.
    /// </summary>    
    public string Login { get; set; } = "Anonymous";

    /// <summary>
    /// Пароль для подключения к FTP-хранилищу.
    /// </summary>
    public string Password { get; set; } = "Anonymous";
  }
}
