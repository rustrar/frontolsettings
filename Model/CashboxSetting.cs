﻿using System.ComponentModel.DataAnnotations;

namespace WpfApp.Model
{
  public class CashboxSetting : PropertyValidateModel
  {
    public int Id { get; set; }
    public int CashboxId { get; set; }
    public Cashbox Cashbox { get; set; }

    public int SettingId { get; set; }
    [Required(ErrorMessage = "Выберите настройку")]
    public Setting Setting { get; set; }
    [Required(ErrorMessage = "Заполните значение настройки")]
    public string Value { get; set; }
  }
}
