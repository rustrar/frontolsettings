﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WpfApp.Model
{
  /// <summary>
  /// Настройка кассы.
  /// </summary>
  public class Setting : PropertyValidateModel
  {
    /// <summary>
    /// Идентификатор.
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Имя настройки.
    /// </summary>
    [Required(ErrorMessage = "Заполните имя настройки")]
    public string Name { get; set; }
    /// <summary>
    /// Кассы.
    /// </summary>
    public List<Cashbox> Cashboxes{ get; set; }
    public List<CashboxSetting> CashboxSettings { get; set; } = new List<CashboxSetting>();
  }
}