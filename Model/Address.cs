﻿using System.ComponentModel.DataAnnotations;

namespace WpfApp.Model
{
  /// <summary>
  /// Адрес.
  /// </summary>
  public class Address : PropertyValidateModel
  {
    /// <summary>
    /// Идентификатор.
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Сокращенное имя адреса.
    /// </summary>
    [Required(ErrorMessage = "Заполните сокращенное имя адреса")]
    public string AbbreviatedName { get; set; }
    /// <summary>
    /// Город.
    /// </summary>
    public string City{ get; set; }
    /// <summary>
    /// Улица.
    /// </summary>
    public string Street { get; set; }
    /// <summary>
    /// Номер дома.
    /// </summary>
    public string Number { get; set; }
  }
}
