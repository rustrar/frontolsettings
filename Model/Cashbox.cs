﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WpfApp.Model
{
  /// <summary>
  /// Касса.
  /// </summary>
  public class Cashbox : PropertyValidateModel
  {
    /// <summary>
    /// Идентификатор.
    /// </summary>
    public int Id { get; set; }
    /// <summary>
    /// Наименование кассы.
    /// </summary>
    [Required(ErrorMessage = "Заполните имя кассы")]
    public string Name { get; set; }
    /// <summary>
    /// Адрес кассы.
    /// </summary>
    [Required(ErrorMessage = "Выберите адрес кассы")]
    public Address Address { get; set; }
    /// <summary>
    /// FTP-хранилище.
    /// </summary>
    [Required(ErrorMessage = "Выберите FTP-соединения")]
    public FTPStorage FTPStorage { get; set; }
    /// <summary>
    /// Настройки.
    /// </summary>
    public List<Setting> Settings{ get; set; }

    public List<CashboxSetting> CashboxSettings { get; set; } = new List<CashboxSetting>();
    /// <summary>
    /// Флаг применения настроек.
    /// </summary>
    public bool IsSuccessApplied { get; set; }
  }
}