﻿namespace WpfApp.Model
{
  public class Message : IMessage
  {
    public object Data { get; set; }
    public Message(object data)
    {
      Data = data;
    }
  }
}