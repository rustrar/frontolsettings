﻿using System;
using System.IO;

namespace WpfApp.Model
{
  public class FileUtils
  {
    public FileStream CreateFile(string fileName = "settings")
    {
      FileInfo fileInfo = new FileInfo(fileName);
      return fileInfo.Create();
    }

    public async void Write(string path, string text)
    {
      try
      {
        using (StreamWriter sw = new StreamWriter(path, true, System.Text.Encoding.Default))        
          await sw.WriteLineAsync(text);        
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
    }

    public async void Read(string path)
    {
      try
      {
        // асинхронное чтение
        using (StreamReader sr = new StreamReader(path))
        {
          Console.WriteLine(await sr.ReadToEndAsync());
        }
      }
      catch (Exception e)
      {
        Console.WriteLine(e.Message);
      }
    }
  }
}
