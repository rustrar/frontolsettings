﻿namespace WpfApp.Model
{
  public interface IFileService
  {
    void Read(string settingPath);

    void Parse(string line);

    void Save(string setting);    
  }
}
