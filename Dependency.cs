﻿using Microsoft.Extensions.DependencyInjection;
using WpfApp.Data;
using WpfApp.Model;
using WpfApp.Providers;
using WpfApp.Services;
using WpfApp.ViewModel;

namespace WpfApp
{
  public static class Dependency
  {
    private static readonly ServiceProvider _provider;
    static Dependency()
    {
      var services = new ServiceCollection();

      services.AddTransient<MainViewModel>();
      services.AddTransient<MainWindow>();
      services.AddTransient<FTPPageViewModel>();
      services.AddScoped<EditFtpPageViewModel>();
      services.AddTransient<SettingPageViewModel>();
      services.AddScoped<EditSettingPageViewModel>();
      services.AddTransient<CashboxPageViewModel>();
      services.AddScoped<EditCashboxPageViewModel>();
      services.AddTransient<AddressPageViewModel>();
      services.AddScoped<EditAddressPageViewModel>();
      services.AddScoped<EditCashboxSettingPageViewModel>();
      services.AddScoped<AddCashboxSettingPageViewModel>();

      services.AddSingleton<SQLiteDBContext>();
      services.AddSingleton<PageService>();
      services.AddSingleton<MessageBus>();
      services.AddSingleton<UnitOfWork>();
      services.AddSingleton<FtpService>();
      services.AddSingleton<FileService>();
      services.AddSingleton<IRepository<Setting>, EFSettingRepository>();
      services.AddSingleton<IRepository<FTPStorage>, EFFTPStorageRepository>();
      services.AddSingleton<IRepository<Cashbox>, EFCashboxRepository>();
      services.AddSingleton<IRepository<Address>, EFAddressRepository>();
      services.AddSingleton<IRepository<CashboxSetting>, EFCashboSettingRepository>();

      _provider = services.BuildServiceProvider();

      foreach (var item in services)
      {
        _provider.GetRequiredService(item.ServiceType);
      }
    }

    public static T Resolve<T>() => _provider.GetRequiredService<T>();
  }
}