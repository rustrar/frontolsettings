﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using WpfApp.Model;

namespace WpfApp.Providers
{
  public class FtpService
  {
		private string password;
		private string userName;
		private string uri;
		private int bufferSize = 1024;

		public bool Passive = true;
		public bool Binary = true;
		public bool EnableSsl = false;
		public bool Hash = false;

		public FtpService() { }

		public FtpService(string uri, string userName, string password)
		{
			this.uri = uri;
			this.userName = userName;
			this.password = password;
		}

		public void Init(FTPStorage fTPStorage)
		{
			this.uri = fTPStorage.Url;
			this.userName = fTPStorage.Login;
			this.password = fTPStorage.Password;
		}


		public async Task<string> ChangeWorkingDirectory(string path)
		{
			uri = combine(uri, path);

			return await PrintWorkingDirectory();
		}

		public async Task<string> DeleteFile(string fileName)
		{
			var request = createRequest(combine(uri, fileName), WebRequestMethods.Ftp.DeleteFile);

			return await GetStatusDescriptionAsync (request);
		}

		public string DownloadFile(string source, string dest)
		{
			var request = createRequest(combine(uri, source), WebRequestMethods.Ftp.DownloadFile);

			byte[] buffer = new byte[bufferSize];

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				using (var stream = response.GetResponseStream())
				{
					using (var fs = new FileStream(dest, FileMode.OpenOrCreate))
					{
						int readCount = stream.Read(buffer, 0, bufferSize);

						while (readCount > 0)
						{
							if (Hash)
								Console.Write("#");

							fs.Write(buffer, 0, readCount);
							readCount = stream.Read(buffer, 0, bufferSize);
						}
					}
				}

				return response.StatusDescription;
			}
		}

		public DateTime GetDateTimestamp(string fileName)
		{
			var request = createRequest(combine(uri, fileName), WebRequestMethods.Ftp.GetDateTimestamp);

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				return response.LastModified;
			}
		}

		public long GetFileSize(string fileName)
		{
			var request = createRequest(combine(uri, fileName), WebRequestMethods.Ftp.GetFileSize);

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				return response.ContentLength;
			}
		}

		public string[] ListDirectory()
		{
			var list = new List<string>();

			var request = createRequest(WebRequestMethods.Ftp.ListDirectory);

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				using (var stream = response.GetResponseStream())
				{
					using (var reader = new StreamReader(stream, true))
					{
						while (!reader.EndOfStream)
						{
							list.Add(reader.ReadLine());
						}
					}
				}
			}

			return list.ToArray();
		}

		public string[] ListDirectoryDetails()
		{
			var list = new List<string>();

			var request = createRequest(WebRequestMethods.Ftp.ListDirectoryDetails);

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				using (var stream = response.GetResponseStream())
				{
					using (var reader = new StreamReader(stream, true))
					{
						while (!reader.EndOfStream)
						{
							list.Add(reader.ReadLine());
						}
					}
				}
			}

			return list.ToArray();
		}

		public async Task<string> MakeDirectory(string directoryName)
		{
			var request = createRequest(combine(uri, directoryName), WebRequestMethods.Ftp.MakeDirectory);

			return await GetStatusDescriptionAsync(request);
		}

		public async Task<string> PrintWorkingDirectory()
		{
			var request = createRequest(WebRequestMethods.Ftp.PrintWorkingDirectory);

			return await GetStatusDescriptionAsync(request);
		}

		public async Task<string> RemoveDirectory(string directoryName)
		{
			var request = createRequest(combine(uri, directoryName), WebRequestMethods.Ftp.RemoveDirectory);

			return await GetStatusDescriptionAsync(request);
		}

		public async Task<string> Rename(string currentName, string newName)
		{
			var request = createRequest(combine(uri, currentName), WebRequestMethods.Ftp.Rename);

			request.RenameTo = newName;

			return await GetStatusDescriptionAsync(request);
		}

		public async Task<string> UploadFile(string source, string destination)
		{
			var request = createRequest(combine(uri, destination), WebRequestMethods.Ftp.UploadFile);

			using (var stream = request.GetRequestStream())
			{
				using (var fileStream = System.IO.File.Open(source, FileMode.Open))
				{
					int num;

					byte[] buffer = new byte[bufferSize];

					while ((num = fileStream.Read(buffer, 0, buffer.Length)) > 0)
					{
						if (Hash)
							Console.Write("#");

						stream.Write(buffer, 0, num);
					}
				}
			}

			return await GetStatusDescriptionAsync(request);
		}

		public async Task<Tuple<bool, string>> CheckConnectionAsync()
		{
      try
      {
        var request = this.createRequest(uri, WebRequestMethods.Ftp.ListDirectory);
        var responseCode = await this.GetStatusDescriptionAsync(request);
				return new Tuple<bool, string>(true, responseCode);
      }
      catch (Exception e)
      {
				return new Tuple<bool, string>(false, e.Message);
      }
		}

		public string UploadFileWithUniqueName(string source)
		{
			var request = createRequest(WebRequestMethods.Ftp.UploadFileWithUniqueName);

			using (var stream = request.GetRequestStream())
			{
				using (var fileStream = System.IO.File.Open(source, FileMode.Open))
				{
					int num;

					byte[] buffer = new byte[bufferSize];

					while ((num = fileStream.Read(buffer, 0, buffer.Length)) > 0)
					{
						if (Hash)
							Console.Write("#");

						stream.Write(buffer, 0, num);
					}
				}
			}

			using (var response = (FtpWebResponse)request.GetResponse())
			{
				return Path.GetFileName(response.ResponseUri.ToString());
			}
		}

		private FtpWebRequest createRequest(string method)
		{
			return createRequest(uri, method);
		}

		private FtpWebRequest createRequest(string uri, string method)
		{
			var r = (FtpWebRequest)WebRequest.Create(uri);

			r.Credentials = new NetworkCredential(userName, password);
			r.Method = method;
			r.UseBinary = Binary;
			r.EnableSsl = EnableSsl;
			r.UsePassive = Passive;

			return r;
		}

    private string GetStatusDescription(FtpWebRequest request)
    {
      using (var response = request.GetResponse())
      {
        return ((FtpWebResponse)response).StatusDescription;
      }
    }

    private async Task<string> GetStatusDescriptionAsync(FtpWebRequest request)
		{
      using (var response = await request.GetResponseAsync())
      {
        return ((FtpWebResponse)response).StatusDescription;
      }
		}

		private string combine(string path1, string path2)
		{
			return Path.Combine(path1, path2).Replace("\\", "/");
		}
	}
}
