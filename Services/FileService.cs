﻿using System;
using System.Collections.Generic;
using System.IO;
using WpfApp.Model;

namespace WpfApp.Services
{
  public class FileService //: IFileService
  {
    private const string _sectionStartSymbol = "$$$";
    private readonly string _settingSectionName = $"{_sectionStartSymbol}ADDSETTINGS";
    
    public void Read(string path)
    {
      using (var reader = File.OpenText(path))
      {
        foreach (var line in Parse(reader))
        {
          System.Diagnostics.Debug.WriteLine(line);
        }
      }
    }

    public IEnumerable<string> Parse(StreamReader reader)
    {
      string line;      
      List<string> result = new List<string>();
      while ((line = reader.ReadLine()) != null)
      {
        if (this.IsSettingsSection(line))        
          result.AddRange(ReadSettingsSection(reader));
      }
      return result;
    }

    private List<string> ReadSettingsSection(StreamReader reader)
    {
      string line;
      List<string> result = new List<string>();
      while ((line = reader.ReadLine()) != null)
      {
        if (line.StartsWith(_sectionStartSymbol))
          break;
        result.Add(line);
      }
      return result;
    }

    public void Save(string setting)
    {
      throw new System.NotImplementedException();
    }

    private bool IsSettingsSection(string line)
    {
      return line.StartsWith(_settingSectionName);
    }
  }
}
