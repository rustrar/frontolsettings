﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;

namespace WpfApp.Services
{
  public class PageService
  {
    public event Action<Page> OnPageChanged;
    public bool CanGoToBack => _history.Skip(1).Any();
    
    public void Navigate(Page page)
    {
      OnPageChanged?.Invoke(page);
      _history.Push(page.GetType());
    }

    private Stack<Type> _history;

    public PageService()
    {
      _history = new Stack<Type>();

    }

    internal void GoToBack()
    {
      _history.Pop();
      OnPageChanged?.Invoke((Page)Activator.CreateInstance(_history.Peek()));
    }
  }
}