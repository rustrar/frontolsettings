﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace WpfApp.Pages
{
  /// <summary>
  /// Interaction logic for FtpPage.xaml
  /// </summary>
  public partial class FtpPage : Page
  {
    public FtpPage()
    {
      InitializeComponent();
    }

    private void Button_Click(object sender, RoutedEventArgs e)
    {
      var listViewItem = GetAncestorByType(e.OriginalSource as DependencyObject, typeof(ListViewItem)) as ListViewItem;
      if (listViewItem != null)
      {
        storagesList.SelectedIndex =
            storagesList.ItemContainerGenerator.IndexFromContainer(listViewItem);
      }
    }

    public static DependencyObject GetAncestorByType(DependencyObject element, Type type)
    {
      if (element == null) 
        return null;

      if (element.GetType() == type) 
        return element;

      return GetAncestorByType(VisualTreeHelper.GetParent(element), type);
    }
  }
}
