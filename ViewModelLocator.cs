﻿using WpfApp.ViewModel;

namespace WpfApp
{
  public class ViewModelLocator
  {
    public MainViewModel MainViewModel => Dependency.Resolve<MainViewModel>();
    public FTPPageViewModel FTPPageViewModel => Dependency.Resolve<FTPPageViewModel>();
    public EditFtpPageViewModel EditFtpPageViewModel => Dependency.Resolve<EditFtpPageViewModel>();
    public SettingPageViewModel SettingPageViewModel => Dependency.Resolve<SettingPageViewModel>();
    public EditSettingPageViewModel EditSettingPageViewModel => Dependency.Resolve<EditSettingPageViewModel>();
    public CashboxPageViewModel CashboxPageViewModel => Dependency.Resolve<CashboxPageViewModel>();
    public EditCashboxPageViewModel EditCashboxPageViewModel => Dependency.Resolve<EditCashboxPageViewModel>();
    public AddressPageViewModel AddressPageViewModel => Dependency.Resolve<AddressPageViewModel>();
    public EditAddressPageViewModel EditAddressPageViewModel => Dependency.Resolve<EditAddressPageViewModel>();
    public EditCashboxSettingPageViewModel EditCashboxSettingPageViewModel => Dependency.Resolve<EditCashboxSettingPageViewModel>();
    public AddCashboxSettingPageViewModel AddCashboxSettingPageViewModel => Dependency.Resolve<AddCashboxSettingPageViewModel>();
  }
}
