﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WpfApp.Migrations
{
    public partial class Add_id_in_CashboxSettings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CashboxSettings",
                table: "CashboxSettings");

            migrationBuilder.AddColumn<int>(
                name: "Id",
                table: "CashboxSettings",
                type: "INTEGER",
                nullable: false,
                defaultValue: 0)
                .Annotation("Sqlite:Autoincrement", true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_CashboxSettings",
                table: "CashboxSettings",
                column: "Id");

            migrationBuilder.CreateIndex(
                name: "IX_CashboxSettings_CashboxId",
                table: "CashboxSettings",
                column: "CashboxId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_CashboxSettings",
                table: "CashboxSettings");

            migrationBuilder.DropIndex(
                name: "IX_CashboxSettings_CashboxId",
                table: "CashboxSettings");

            migrationBuilder.DropColumn(
                name: "Id",
                table: "CashboxSettings");

            migrationBuilder.AddPrimaryKey(
                name: "PK_CashboxSettings",
                table: "CashboxSettings",
                columns: new[] { "CashboxId", "SettingId" });
        }
    }
}
