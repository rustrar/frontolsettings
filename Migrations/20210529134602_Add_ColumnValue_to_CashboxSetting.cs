﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WpfApp.Migrations
{
    public partial class Add_ColumnValue_to_CashboxSetting : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashboxSetting");

            migrationBuilder.CreateTable(
                name: "CashboxSettings",
                columns: table => new
                {
                    CashboxId = table.Column<int>(type: "INTEGER", nullable: false),
                    SettingId = table.Column<int>(type: "INTEGER", nullable: false),
                    Value = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashboxSettings", x => new { x.CashboxId, x.SettingId });
                    table.ForeignKey(
                        name: "FK_CashboxSettings_Cashboxes_CashboxId",
                        column: x => x.CashboxId,
                        principalTable: "Cashboxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CashboxSettings_Settings_SettingId",
                        column: x => x.SettingId,
                        principalTable: "Settings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CashboxSettings_SettingId",
                table: "CashboxSettings",
                column: "SettingId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashboxSettings");

            migrationBuilder.CreateTable(
                name: "CashboxSetting",
                columns: table => new
                {
                    CashboxesId = table.Column<int>(type: "INTEGER", nullable: false),
                    SettingsId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashboxSetting", x => new { x.CashboxesId, x.SettingsId });
                    table.ForeignKey(
                        name: "FK_CashboxSetting_Cashboxes_CashboxesId",
                        column: x => x.CashboxesId,
                        principalTable: "Cashboxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CashboxSetting_Settings_SettingsId",
                        column: x => x.SettingsId,
                        principalTable: "Settings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CashboxSetting_SettingsId",
                table: "CashboxSetting",
                column: "SettingsId");
        }
    }
}
