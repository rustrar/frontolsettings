﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WpfApp.Migrations
{
    public partial class Add_Cashboxes_to_Settings : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Settings_Cashboxes_CashboxId",
                table: "Settings");

            migrationBuilder.DropIndex(
                name: "IX_Settings_CashboxId",
                table: "Settings");

            migrationBuilder.DropColumn(
                name: "CashboxId",
                table: "Settings");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Settings",
                type: "TEXT",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "TEXT",
                oldNullable: true);

            migrationBuilder.CreateTable(
                name: "CashboxSetting",
                columns: table => new
                {
                    CashboxesId = table.Column<int>(type: "INTEGER", nullable: false),
                    SettingsId = table.Column<int>(type: "INTEGER", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_CashboxSetting", x => new { x.CashboxesId, x.SettingsId });
                    table.ForeignKey(
                        name: "FK_CashboxSetting_Cashboxes_CashboxesId",
                        column: x => x.CashboxesId,
                        principalTable: "Cashboxes",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_CashboxSetting_Settings_SettingsId",
                        column: x => x.SettingsId,
                        principalTable: "Settings",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_CashboxSetting_SettingsId",
                table: "CashboxSetting",
                column: "SettingsId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "CashboxSetting");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "Settings",
                type: "TEXT",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "TEXT");

            migrationBuilder.AddColumn<int>(
                name: "CashboxId",
                table: "Settings",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Settings_CashboxId",
                table: "Settings",
                column: "CashboxId");

            migrationBuilder.AddForeignKey(
                name: "FK_Settings_Cashboxes_CashboxId",
                table: "Settings",
                column: "CashboxId",
                principalTable: "Cashboxes",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
