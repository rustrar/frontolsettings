﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WpfApp.Migrations
{
    public partial class ftp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FTPStorageId",
                table: "Cashboxes",
                type: "INTEGER",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "FTPStorages",
                columns: table => new
                {
                    Id = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Name = table.Column<string>(type: "TEXT", nullable: true),
                    Url = table.Column<string>(type: "TEXT", nullable: true),
                    Port = table.Column<int>(type: "INTEGER", nullable: false),
                    Login = table.Column<string>(type: "TEXT", nullable: true),
                    Password = table.Column<string>(type: "TEXT", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_FTPStorages", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cashboxes_FTPStorageId",
                table: "Cashboxes",
                column: "FTPStorageId");

            migrationBuilder.AddForeignKey(
                name: "FK_Cashboxes_FTPStorages_FTPStorageId",
                table: "Cashboxes",
                column: "FTPStorageId",
                principalTable: "FTPStorages",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Cashboxes_FTPStorages_FTPStorageId",
                table: "Cashboxes");

            migrationBuilder.DropTable(
                name: "FTPStorages");

            migrationBuilder.DropIndex(
                name: "IX_Cashboxes_FTPStorageId",
                table: "Cashboxes");

            migrationBuilder.DropColumn(
                name: "FTPStorageId",
                table: "Cashboxes");
        }
    }
}
